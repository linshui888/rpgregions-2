plugins {
    id("buildlogic.java-common-conventions")
}

val ultraRegionsSupport = (properties.getValue("ultraRegionsSupport") as String).toBoolean()

tasks.compileJava {
    if (!ultraRegionsSupport) {
        sourceSets.main.get().java.exclude("**/net/islandearth/rpgregions/api/integrations/ultraregions/UltraRegionsIntegration.java")
    }
}

dependencies {
    implementation(project(":api"))

    compileOnly("org.spigotmc:spigot-api:${properties["spigot_version"]}")
    compileOnly("com.sk89q.worldguard:worldguard-bukkit:${properties["worldguard_version"]}") {
        exclude("com.destroystokyo.paper")
        exclude("org.spigotmc")
    }
    compileOnly(":Residence4.9.2.2") // residence
    compileOnly("com.github.GriefPrevention:GriefPrevention:16.18.2") // griefprevention
    compileOnly("com.github.angeschossen:LandsAPI:6.37.0") // lands
    compileOnly("com.griefdefender:api:2.1.0-SNAPSHOT") // GriefDefender
    if (ultraRegionsSupport) compileOnly(":UltraRegions") // ultraregions
    compileOnly("org.empirewar.orbis:paper-api:1.0.0-SNAPSHOT")
}
