plugins {
    // Apply the java Plugin to add support for Java.
    java
    id("io.github.goooler.shadow")
    id("com.diffplug.spotless")
//    id("net.kyori.indra.licenser.spotless")
}

spotless {
    java {
        toggleOffOn("@formatter:off", "@formatter:on")
        trimTrailingWhitespace()
        formatAnnotations()
        removeUnusedImports()
        endWithNewline()
        toggleOffOn()
    }

    kotlinGradle {
        endWithNewline()
        indentWithSpaces(4)
        trimTrailingWhitespace()
    }
}

repositories {
    mavenCentral()
    mavenLocal()

    maven("https://s01.oss.sonatype.org/content/repositories/snapshots/")
    maven("https://hub.spigotmc.org/nexus/content/repositories/snapshots/")
    maven("https://repo.codemc.io/repository/maven-snapshots/")
    maven("https://jitpack.io")
    maven("https://repo.convallyria.com/releases")
    maven("https://repo.convallyria.com/snapshots")
    maven("https://repo.papermc.io/repository/maven-public/")
    maven("https://oss.sonatype.org/content/groups/public/")
    maven("https://repo.aikar.co/content/groups/aikar/")

    // worldguard
    maven("https://maven.enginehub.org/repo/")

    // PAPI
    maven("https://repo.extendedclip.com/content/repositories/placeholderapi/")

    // MythicMobs
    maven("https://mvn.lumine.io/repository/maven-public/")

    // Dynmap
    maven("https://repo.mikeprimm.com")

    // GriefDefender
    maven("https://repo.glaremasters.me/repository/bloodshot")

    // Crunch
    maven("https://redempt.dev")

    flatDir { dir("../libraries") }
}

project.group = "net.islandearth.rpgregions"
project.version = "1.4.9"

group = project.group
version = project.version

dependencies {
    constraints {
        // Define dependency versions as constraints
        compileOnly("net.kyori:adventure-api:4.17.0")
        implementation("net.kyori:adventure-api:4.17.0")
        testImplementation("net.kyori:adventure-api:4.17.0")
    }

    implementation("com.convallyria.languagy:api:3.0.3-SNAPSHOT")
    implementation("com.google.code.gson:gson:2.10.1")

//    compileOnly("net.kyori:adventure-api:4.17.0")
//    compileOnly("net.kyori:adventure-text-serializer-plain:4.17.0")
    compileOnly("net.kyori:adventure-platform-bukkit:4.3.4") // IMPLEMENTED VIA LIBRARIES
//    implementation("net.kyori:adventure-platform-api:4.3.3-SNAPSHOT")
    compileOnly("net.kyori:adventure-text-minimessage:4.14.0") // IMPLEMENTED VIA LIBRARIES
}

testing {
    suites {
        // Configure the built-in test suite
        val test by getting(JvmTestSuite::class) {
            // Use JUnit Jupiter test framework
            useJUnitJupiter("5.10.1")
        }
    }
}

// Apply a specific Java toolchain to ease working on different environments.
java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(21)
    }
    disableAutoTargetJvm()
}

tasks {
    processResources {
        filesMatching("plugin.yml") {
            expand("version" to project.version)
        }
    }

    compileJava {
        options.encoding = Charsets.UTF_8.name()
        options.release.set(16)
    }

    build {
        dependsOn(shadowJar)
    }

    shadowJar {
        archiveBaseName.set("rpgregions-2-${project.name}")
        archiveClassifier.set("")
//        relocate("net.kyori", "rpgregions-libs.adventure")
        relocate("com.google.gson", "rpgregions-libs.gson")
        relocate("org.incendo.cloud", "rpgregions-libs.commandframework")
        relocate("io.leangen.geantyref", "rpgregions-libs.typetoken")
        relocate("com.convallyria.languagy", "rpgregions-libs.languagy")
        relocate("io.papermc.lib", "rpgregions-libs.paperlib")
        relocate("redempt.crunch", "rpgregions-libs.crunch")
        relocate("co.aikar.idb", "rpgregions-libs.idb")
        relocate("com.github.stefvanschie.inventoryframework", "rpgregions-libs.inventoryframework")
        relocate("org.bstats", "rpgregions-libs.bstats")
        relocate("me.lucko.helper", "rpgregions-libs.helper")
        relocate("net.wesjd", "rpgregions-libs.anvilgui")
    }
}