CREATE TABLE IF NOT EXISTS rpgregions_timed_discoveries (uuid varchar(32) NOT NULL, region varchar(32) NOT NULL, start bigint NOT NULL, latest bigint, PRIMARY KEY(uuid, region));
