package net.islandearth.rpgregions.commands.parser;

import com.google.common.collect.ImmutableList;
import net.islandearth.rpgregions.api.RPGRegionsAPI;
import net.islandearth.rpgregions.commands.caption.RPGRegionsCaptionKeys;
import net.islandearth.rpgregions.managers.data.region.ConfiguredRegion;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.incendo.cloud.caption.CaptionVariable;
import org.incendo.cloud.context.CommandContext;
import org.incendo.cloud.context.CommandInput;
import org.incendo.cloud.exception.parsing.ParserException;
import org.incendo.cloud.parser.ArgumentParseResult;
import org.incendo.cloud.parser.ArgumentParser;
import org.incendo.cloud.suggestion.BlockingSuggestionProvider;

import java.util.Optional;

public final class ConfiguredRegionArgument<C> implements ArgumentParser<C, ConfiguredRegion>, BlockingSuggestionProvider.Strings<C> {

    @Override
    public @NonNull ArgumentParseResult<@NonNull ConfiguredRegion> parse(@NonNull CommandContext<@NonNull C> commandContext, @NonNull CommandInput commandInput) {
        final String input = commandInput.peekString();

        final Optional<ConfiguredRegion> configuredRegion = RPGRegionsAPI.getAPI().getManagers().getRegionsCache().getConfiguredRegion(input);
        if (configuredRegion.isPresent()) {
            commandInput.readString();
            return ArgumentParseResult.success(configuredRegion.get());
        }
        return ArgumentParseResult.failure(new ConfiguredRegionParserException(input, commandContext));
    }

    @Override
    public @NonNull Iterable<@NonNull String> stringSuggestions(@NonNull CommandContext<C> commandContext, @NonNull CommandInput input) {
        return ImmutableList.copyOf(RPGRegionsAPI.getAPI().getManagers().getRegionsCache().getConfiguredRegions().keySet());
    }

    public static final class ConfiguredRegionParserException extends ParserException {

        public ConfiguredRegionParserException(
                final @NonNull String input,
                final @NonNull CommandContext<?> context
        ) {
            super(
                    ConfiguredRegionArgument.class,
                    context,
                    RPGRegionsCaptionKeys.ARGUMENT_PARSE_FAILURE_REGION_NOT_FOUND,
                    CaptionVariable.of("input", input)
            );
        }
    }
}
