package net.islandearth.rpgregions.commands.parser;

import com.google.common.collect.ImmutableList;
import net.islandearth.rpgregions.api.RPGRegionsAPI;
import net.islandearth.rpgregions.api.integrations.rpgregions.RPGRegionsIntegration;
import net.islandearth.rpgregions.api.integrations.rpgregions.region.RPGRegionsRegion;
import net.islandearth.rpgregions.commands.caption.RPGRegionsCaptionKeys;
import org.bukkit.entity.Player;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.incendo.cloud.caption.CaptionVariable;
import org.incendo.cloud.context.CommandContext;
import org.incendo.cloud.context.CommandInput;
import org.incendo.cloud.exception.parsing.ParserException;
import org.incendo.cloud.parser.ArgumentParseResult;
import org.incendo.cloud.parser.ArgumentParser;
import org.incendo.cloud.suggestion.BlockingSuggestionProvider;

import java.util.List;
import java.util.Optional;

public final class IntegrationRegionArgument<C> implements ArgumentParser<C, RPGRegionsRegion>, BlockingSuggestionProvider.Strings<C> {

    @Override
    public @NonNull ArgumentParseResult<@NonNull RPGRegionsRegion> parse(@NonNull CommandContext<@NonNull C> commandContext, @NonNull CommandInput commandInput) {
        final String input = commandInput.peekString();

        if (RPGRegionsAPI.getAPI().getManagers().getIntegrationManager() instanceof RPGRegionsIntegration rpgRegionsIntegration) {
            Optional<RPGRegionsRegion> region = rpgRegionsIntegration.getRegion(input);
            if (region.isPresent()) {
                commandInput.readString();
                return ArgumentParseResult.success(region.get());
            }
        }

        return ArgumentParseResult.failure(new IntegrationRegionParserException(input, commandContext));
    }

    @Override
    public @NonNull Iterable<@NonNull String> stringSuggestions(@NonNull CommandContext<C> commandContext, @NonNull CommandInput input) {
        if (commandContext.sender() instanceof Player player) {
            return ImmutableList.copyOf(RPGRegionsAPI.getAPI().getManagers().getIntegrationManager().getAllRegionNames(player.getWorld()));
        }
        return List.of();
    }

    public static final class IntegrationRegionParserException extends ParserException {

        public IntegrationRegionParserException(
                final @NonNull String input,
                final @NonNull CommandContext<?> context
        ) {
            super(
                    RPGRegionsRegion.class,
                    context,
                    RPGRegionsCaptionKeys.ARGUMENT_PARSE_FAILURE_REGION_NOT_FOUND,
                    CaptionVariable.of("input", input)
            );
        }
    }
}
