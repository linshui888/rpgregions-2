package net.islandearth.rpgregions.commands.caption;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.incendo.cloud.caption.CaptionProvider;
import org.incendo.cloud.caption.DelegatingCaptionProvider;

/**
 * Caption registry that uses bi-functions to produce messages
 *
 * @param <C> Command sender type
 */
public class RPGRegionsCaptionProvider<C> extends DelegatingCaptionProvider<C> {

    /**
     * Default caption for {@link RPGRegionsCaptionKeys#ARGUMENT_PARSE_FAILURE_REGION_NOT_FOUND}.
     */
    public static final String ARGUMENT_PARSE_FAILURE_REGION_NOT_FOUND = "Could not find region '{input}'";

    /**
     * Default caption for {@link RPGRegionsCaptionKeys#ARGUMENT_PARSE_FAILURE_FAUNA_NOT_FOUND}.
     */
    public static final String ARGUMENT_PARSE_FAILURE_FAUNA_NOT_FOUND = "Could not find fauna '{input}'";

    private static final CaptionProvider<?> PROVIDER = CaptionProvider.constantProvider()
            .putCaption(
                    RPGRegionsCaptionKeys.ARGUMENT_PARSE_FAILURE_FAUNA_NOT_FOUND,
                    ARGUMENT_PARSE_FAILURE_FAUNA_NOT_FOUND
            ).putCaption(
                    RPGRegionsCaptionKeys.ARGUMENT_PARSE_FAILURE_REGION_NOT_FOUND,
                    ARGUMENT_PARSE_FAILURE_REGION_NOT_FOUND
            )
            .build();

    @Override
    public @NonNull CaptionProvider<C> delegate() {
        return (CaptionProvider<C>) PROVIDER;
    }
}
